# Viberr - a cloud music app #

Viberr is an application that let's you upload, store, and play all of your music from the cloud. You can now manage and listen to your music from any device, anywhere in the world. I made it as a learning lesson for Django.

![album_index](screenshots/album_index.JPG)

### Register/Login ###
To get started, you have to create an account and log in. Later you have to create an album.

![register](screenshots/register.JPG)

### Adding Songs ###
After an album is created you will then be able to add/upload songs. 

![add_song](screenshots/add_song.JPG)

### My Songs ###
Once songs are added to an album you are then able to play, favorite, and delete them.

![album_detail](screenshots/album_detail.JPG)